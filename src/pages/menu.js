import React from 'react'
import Link from 'gatsby-link'
import StaticMenu from '../components/StaticMenu/index'

const MenuPage = () => (
  <div>
    <h1>Corporal Dee's BBQ Menu</h1>
    <p>Corporal Dee's Menu</p>
    <StaticMenu/>
    <Link to="/">Go back to the homepage</Link>
  </div>
)

export default MenuPage
