import React from 'react'
import Link from 'gatsby-link'
import LocationMap from '../components/LocationMap'

const LocationPage = () => (
  <div>
    <h1>Our Location</h1>
    <p>Corporal Dee's Location</p>
    <Link to="//bit.ly/corporal-dee-location" target="_blank">Our Location on Google Maps</Link>
    <LocationMap></LocationMap>
    <Link to="/">Go back to the homepage</Link>
  </div>
)

export default LocationPage
