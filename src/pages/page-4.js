import React from 'react'
import Link from 'gatsby-link'

const FourthPage = () => (
  <div>
    <h1>Hi from the FOURTH  page</h1>
    <p>Welcome to page 4</p>
    <Link to="/">Go back to the homepage</Link>
  </div>
)

export default FourthPage
