import React from 'react'
import Link from 'gatsby-link'
import CateringMenu from '../components/CateringMenu/index'

const MenuPage = () => (
  <div>
    <h1>Corporal Dee's Catering Menu</h1>
    <p>Corporal Dee's Menu</p>
    <CateringMenu/>
    <Link to="/">Go back to the homepage</Link>
  </div>
)

export default MenuPage
