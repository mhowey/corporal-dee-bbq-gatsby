import React from 'react';
import Link from 'gatsby-link';

const IndexPage = ({data}) => (
  <div>
    <h1>Welcome to {data.site.siteMetadata.title}!</h1>
    {/*<p>{data.site.siteMetadata.description}</p>*/}
    <p>We served our country, now we want to serve you! You won't find better
    BBQ anywhere in Pennsylvania than you can find right here in Exton, PA at Corporal Dee BBQ!</p>
    <p>Call us @ {data.site.siteMetadata.phone}</p>
    <p>Want to see what great food we offer? Go check out our <Link to="/menu">Menu here</Link>.</p>
    <p>Ready to come get some? <Link to="location">Go to our location page to find your way!</Link></p>
  </div>
)
//<Link to="/page-2/">Go to page 2</Link>
export default IndexPage

/**
 * Our GraphQL 
 */
export const query = graphql`
query SiteInfo {
  site {
    siteMetadata{
      title
      description
      phone
    }
  }
  bgimg: imageSharp(id: {regex: "/something-else.jpeg/"}) {
    sizes(maxWidth: 1240) {
      ...GatsbyImageSharpSizes
    }
  }
  anotherImage: imageSharp(id: {regex: "/anotherimage.jpeg/"}) {
    sizes(maxWidth: 1240) {
      ...GatsbyImageSharpSizes
    }
  }
  circuitBoard: imageSharp(id: {regex: "/unsplashcircuitboard.jpeg/"}) {
    sizes(maxWidth: 1240) {
      ...GatsbyImageSharpSizes
    }
  }
}
`