import React from "react";
// import Link from "gatsby-link";
import styled from "styled-components";
import Img from "gatsby-image";

import catering1 from "../../images/corp-dee-catering-menu1.jpg"
import catering2 from "../../images/corp-dee-catering-menu2.jpg"

const CateringMenuWrapper = styled.div`
  width:100%;
  // height:300px;
  // border: 1px dotted #ccc;
  vertical-align: top;
  div {
    // width: 50%;
    img {
      width: 700px;
    }
  }
`;

const CateringMenu = ({data}) => (
  <CateringMenuWrapper>
    <div><Img src={catering1} /></div>
    <div><Img src={catering2} /></div>
  </CateringMenuWrapper>
);

export default CateringMenu
