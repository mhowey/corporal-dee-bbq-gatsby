import React from "react";
import Link from "gatsby-link";
import styled from "styled-components";

import cdlogo from "../../images/corp-dee-logo.png";
import headerBackground from "../../images/food-restaurant-picnic-236887.jpeg";

const HeaderWrapper = styled.header`
  margin-bottom: 1.45rem;
`;

const BrandBanner = styled.div`
  padding: 10px;
  background-image: url(${headerBackground});
  background-size: cover;
  background-position-y: -205px;
  height: 180px;
  a {
    color: #ffffff;
  }
  img {
    height: 150px;
    position: relative;
    top: 10px;
    left: 10px;
    margin-bottom: -30px;
  }
  @media (max-width: 768px) {
    padding: 5px;
    background-image: url(${headerBackground});
    background-size: cover;
    background-position-y: -205px;
    height: 125px;
    a {
      color: #ffffff;
    }
    img {
      height: 100px;
      position: relative;
      top: 5px;
      left: 5px;
    }
  }
`;

const SiteTitle = styled.div`
  background-repeat: once;
  background-position: right;
  float: right;
  font-family: arial, sans-serif;
  font-size: 28px;
  font-weight: 600;
  .title, .phone {
    text-align: right;
  }
  .title {
    font-size: 32px;
    line-height: 33px;
  }
  .phone {
    font-size: 24px;
  }
  @media (max-width: 768px) {
    .title {
      font-size: 22px;
      line-height: 23px;
    }
    .phone {
      font-size: 18px;
    }
  }
`;

const ResponsiveNavContainer = styled.nav`
  overflow: hidden;
  background-color: #333;
  a { 
    float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 17px;
    font-family: sans-serif;
  }
`;

const Header = ({data}) => (
  <HeaderWrapper>
      <BrandBanner>
        <Link activeClassName="active"
          to="/"
        >
          <img src={cdlogo} alt="Corporal Dee Barbeque Logo" /> <SiteTitle><div className="title">{data.site.siteMetadata.title}</div><div className="phone">{data.site.siteMetadata.phone}</div></SiteTitle>
        </Link>
      </BrandBanner>

    <ResponsiveNavContainer>
        <Link to="/">Home Page</Link>
        <Link to="/menu">BBQ Menu</Link>
        <Link to="/catering">Catering</Link>
        <Link to="/location">Location</Link>
    </ResponsiveNavContainer>
  </HeaderWrapper>
)

export default Header
