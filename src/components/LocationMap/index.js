import React from "react";
import Link from "gatsby-link";
import styled from "styled-components";

const MapWrapper = styled.div`
  width:100%;
  height:300px;
  @media (max-width: 500px) {
    height: 200px;
  }
  iframe {
    border: 1px solid #333333;
    background-color: #cccccc;
    padding:10px;
  }
`;

const LocationMap = ({data}) => (
  <MapWrapper>
    <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3054.2128895631085!2d-75.63916618479638!3d40.04834978588531!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c6f4ba6f08661f%3A0x8cf39394bb04820a!2sCorporal+Dee+BBQ!5e0!3m2!1sen!2sus!4v1524698812530"
      width="99%" height="99%" frameBorder="0" allowFullScreen></iframe>
  </MapWrapper>
);

export default LocationMap
