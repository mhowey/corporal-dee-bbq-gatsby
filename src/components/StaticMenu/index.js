import React from "react";
// import Link from "gatsby-link";
import styled from "styled-components";
import Img from 'gatsby-image';

import menu1 from "../../images/corpdee-menu-page1.jpg"
import menu2 from "../../images/corpdee-menu-page2.jpg"

const StaticMenuWrapper = styled.div`
  width:100%;
  // height:300px;
  // border: 1px dotted #ccc;
  vertical-align: top;
  div {
    // width: 50%;
    img {
      width: 700px;
    }
  }
`;

const StaticMenu = ({data}) => (
  <StaticMenuWrapper>
    <div><Img src={menu1} /></div>
    <div><Img src={menu2} /></div>
  </StaticMenuWrapper>
);

export default StaticMenu
