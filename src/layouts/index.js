import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/Header'
import './index.scss'

const TemplateWrapper = ({ children, data }) => (
  <div>
    <Helmet
      title="Corporal Dee BBQ"
      meta={[
        { name: 'description', content: 'Corporal Dee BBQ has the best BBQ in Pennsylvania' },
        { name: 'keywords', content: 'bbq, exton, pennsylvania, barbeque, Marines' },
      ]}
    />
    <Header data={data} />
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '0px 1.0875rem 1.45rem',
        paddingTop: 0,
      }}
    >
      {children()}
    </div>
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper

export const queryHeader = graphql`
  query SiteInfoHeader {
    site {
    siteMetadata{
      title
      description
      phone
    } 
    }
  }
  `;