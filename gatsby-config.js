module.exports = {
  siteMetadata: {
    title: 'Corporal Dee BBQ',
    description: 'Best BBQ in Pennsylvania!',
    phone: '(610) 594-1832'
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: `posts`,
        path: `${__dirname}/src/posts/`,
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: `img`,
        path: `${__dirname}/src/images/`,
      }
    },
    'gatsby-transformer-remark',
    'gatsby-plugin-styled-components',

    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
  ],
};
